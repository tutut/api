import tornado.escape
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import os.path
import sqlite3
import json

from functools import partial
from tornado.options import define, options
from tornado.escape import json_encode

define("port", group="root", default=8888, help="run on the given port", type=int)
define("listen_address", group="root", default="*", help="run on the given listen address attach")
define("ip", group="root", default="*", help="run on the given listen address")

DB_FILE = os.path.abspath(os.path.join(os.path.dirname(__file__), 'todo.sqlite'))
db_connect = sqlite3.connect(DB_FILE)

class Application(tornado.web.Application):
    def __init__(self):
           handlers = [
               (r"/api", ListHandler),
               (r"/api/([^/]+)", ShowHandler),
               (r"/apis/del", DeleteHandler),
               (r"/apis/post", AddHandler),
           ]

           settings = dict(
               debug=True,
           )
           super(Application, self).__init__(handlers, **settings)


class AddHandler(tornado.web.RequestHandler):
    def post(self):
        cur = db_connect.cursor()
        todo = self.get_body_argument("todo")
        cur.execute("""INSERT INTO todos (todo) VALUES (?)""", (todo,))
        last_id = cur.lastrowid
        cur.connection.commit()
        json = {"id": last_id, "todo": todo}
        return self.write(json_encode(json))

class ListHandler(tornado.web.RequestHandler):
    def get(self):
        cur = db_connect.cursor()
        cur.execute("""SELECT id, todo FROM todos""")
        todos = [{"id": id, "todo": todo} for id, todo in cur]

        return self.write(json_encode(todos))


class ShowHandler(tornado.web.RequestHandler):
    def get(self,argument):
        cur = db_connect.cursor()
        id = int(argument)
        cur.execute("""SELECT id, todo FROM todos WHERE id = ?""", (id,))
        todo = cur.fetchone()
        encode = {"error": "not found"}
        if not todo:
            return self.write(json_encode(encode))
        todo = {"id": todo[0], "todo": todo[1]}

        return self.write(json_encode(todo))


class DeleteHandler(tornado.web.RequestHandler):
    def post(self):
        cur = db_connect.cursor()
        id = int(self.get_body_argument("id"))
        cur.execute("""DELETE FROM todos WHERE id = ?""", (id,))
        encode = {"error": "not found"}
        if not cur.rowcount:
            return self.write(json_encode(encode))
        cur.connection.commit()

        return self.write(json_encode("{}"))


def maybe_create_schema():
    db = db_connect()
    db.execute("""
        CREATE TABLE IF NOT EXISTS todos
        (id INTEGER PRIMARY KEY, todo TEXT)""")
    db.close()


def cursor(request):
    def done_cb(request):
        request.extra['conn'].close()

    if 'conn' not in request.extra:
        request.extra['conn'] = db_connect()
        request.add_done_callback(done_cb)

    return request.extra['conn'].cursor()


def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port,address=options.ip)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()

